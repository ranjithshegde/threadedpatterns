-- Lower bounds
a = 900
-- Upper bounds
b = 2000

-- Pwhite start
start = true

-- Pwhite duration
dur = 300

stop = false

-- Color 1
col1 = {
    r = 100,
    g = 100,
    b = 0,
}

-- Color 2
col2 = {
    r = 100,
    g = 100,
    b = 256,
}

-- c = 150
-- if c >= 100 then
--     start = true
-- end
