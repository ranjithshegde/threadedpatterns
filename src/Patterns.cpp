#include "Patterns.h"

Patterns::Patterns()
    : m_isPlaying(false)
{
}

Patterns::~Patterns()
{
    stop();
    std::for_each(T.begin(), T.end(), std::mem_fn(&std::thread::join));
}

float Patterns::Pwhite(float& begin, float& end)
{
    time_proxy now = std::chrono::steady_clock::now();
    float rand = ofRandom(begin, end);
    time_proxy done = std::chrono::steady_clock::now();
    m_evalTime = done - now;
    return rand;
}

float Patterns::Prand(std::vector<float> list)
{
    int val = ofRandom(list.size());
    return list[val];
}

void Patterns::m_Tick(std::function<float(float&, float&)> func, std::chrono::duration<float> evaltime, unsigned long& milliseconds, std::atomic<float>& newInput, float& beg, float& end)
{
    m_isPlaying = true;
    while (m_isPlaying) {
        newInput = func(beg, end);
        auto x = std::chrono::steady_clock::now() + std::chrono::milliseconds(milliseconds) - evaltime;
        // std::cout << "Thread waiting\n";
        std::this_thread::sleep_until(x);
    }
}

void Patterns::Play(std::function<float(float&, float&)> func, std::chrono::duration<float> evaltime, unsigned long& milliseconds, std::atomic<float>& newInput, float& beg, float& end)
{
    auto tick = [this](std::function<float(float&, float&)> func, std::chrono::duration<float> evaltime, unsigned long& milliseconds, std::atomic<float>& newInput, float& beg, float& end) {
        m_isPlaying = true;
        while (m_isPlaying) {
            newInput = func(beg, end);
            auto x = std::chrono::steady_clock::now() + std::chrono::milliseconds(milliseconds) - evaltime;
            std::this_thread::sleep_until(x);
        }
    };
    T.push_back(std::thread(tick, func, evaltime, std::ref(milliseconds), std::ref(newInput), std::ref(beg), std::ref(end)));
}

dur_proxy& Patterns::evalTime()
{
    return m_evalTime;
}

void Patterns::stop()
{
    m_isPlaying = false;
}

void Patterns::resume()
{
    m_isPlaying = true;
}
