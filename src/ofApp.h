#pragma once
#include "FileWatcher.h"
#include "ofLua.hpp"

#include "Patterns.h"

//! A simple pwhite implementation that sets the frequency of an oscillator.
class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void audioOut(float* buffer, int bufferSize, int nChannels);
    void exit();

    //! An abstraction layer to communicate with RTAudio and initiate sound stream
    ofSoundStream soundStream;
    int sampleRate;
    int bufferSize;

    bool oscStart, end, change;
    double phase, phaseInc, phase_2, phase_2Inc;
    float freq, freq2, amplitude, amp2;
    float mouseX;
    ofPolyline pLine;
    ofMesh quad;

    Patterns patterns;
    unsigned long millis = 1000;

    // double grad_a, grad_b, grad_c;
    ofColor grad_a, grad_b;

    //! Atomics are needed to safely transfer data between multiple threads.
    std::atomic<float> output;
    std::atomic<float> lua_out;
    ofLua luv;
    std::string file = ofToDataPath("lua.lua", true);
    float a, b, c;
};
