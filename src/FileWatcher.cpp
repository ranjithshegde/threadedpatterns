#include "FileWatcher.h"
#include <filesystem>

FileWatcher::FileWatcher(std::string path_to_watch, std::chrono::duration<int, std::milli> delay)
    : path_to_watch(path_to_watch)
    , delay(delay)
{
    for (const auto& file : std::filesystem::recursive_directory_iterator(path_to_watch)) {
        m_Paths[file.path().string()] = std::filesystem::last_write_time(file);
    }
}
void FileWatcher::start(const std::function<void(std::string, FileStatus)>& action)
{
    while (m_Running) {
        std::this_thread::sleep_for(delay);

        auto it = m_Paths.begin();

        while (it != m_Paths.end()) {
            if (!std::filesystem::exists(it->first)) {
                action(it->first, FileStatus::erased);
                it = m_Paths.erase(it);
            } else {
                it++;
            }
        }

        for (const std::filesystem::directory_entry& file : std::filesystem::recursive_directory_iterator(path_to_watch)) {
            std::filesystem::file_time_type current_file_last_write_time = std::filesystem::last_write_time(file);

            if (!m_Contains(file.path().string())) {
                m_Paths[file.path().string()] = current_file_last_write_time;
                action(file.path().string(), FileStatus::created);
            } else {
                if (m_Paths[file.path().string()] != current_file_last_write_time) {
                    m_Paths[file.path().string()] = current_file_last_write_time;
                    action(file.path().string(), FileStatus::modified);
                }
            }
        }
    }
}
