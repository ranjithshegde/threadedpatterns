#pragma once
#include "ofMain.h"

typedef std::chrono::time_point<std::chrono::steady_clock> time_proxy;
typedef std::chrono::duration<float> dur_proxy;

//! A collection of Pattern functions similar to SuperCollider's Pattern/Event library.
//! The main function creates a thread on every call, which runs in the background, sleeping for specified intervals
//! Any pattern engine can be passed to it as a function pointer, which the play function executes
class Patterns {
public:
    //! Default constuctor
    Patterns();

    //! Destructor that frees all threads
    ~Patterns();

    //! Pwhite function from SuperCollider. Generates a random number with standard distribution.
    //! @param begin Lower bounds.
    //! @param end Upper bounds.
    float Pwhite(float& begin, float& end);

    float Prand(std::vector<float> list);

    //! Starts the pattern engine by first creating a new thread, and then passing the values to `Patterns::m_Tick`
    //! @param func A std:;function or a function pointer - The pattern engine to execute on the new thread. It needs the relevant arguments for that function.
    //! @param evaltime A time point instance that represents the duration of execution of the pattern function. Ideally this should be returned directly from the pattern generator.
    //! @param milliseconds Same as Dur value from SuperCollider, the of rest between each value generation from the patternfunc.
    //! @param newInput A variable reference to store the output of the pattern generation.
    //! @param beg Lower bounds for the Pwhite engine.
    //! @param beg Upper bounds for the Pwhite engine.
    void Play(std::function<float(float&, float&)> func, std::chrono::duration<float> evaltime, unsigned long& milliseconds, std::atomic<float>& newInput, float& beg, float& end);

    //! Stop the pattern execution.
    //! Simply sleeps the thread.
    void stop();

    //! Resume pattern execution.
    void resume();

    //! Chrono timepoint to store the evaluation time of the pattern function
    dur_proxy& evalTime();

private:
    dur_proxy m_evalTime;
    std::vector<std::thread> T;
    bool m_isTicking;
    std::vector<double> m_audioBuffer;
    std::atomic<bool> m_isPlaying;

    //! A threaded function that directly takes the values passed to `Pattern::Play` and executes the function pointer within.
    //! @param func A std:;function or a function pointer - The pattern engine to execute on the new thread. It needs the relevant arguments for that function.
    //! @param evaltime A time point instance that represents the duration of execution of the pattern function. Ideally this should be returned directly from the pattern generator.
    //! @param milliseconds Same as Dur value from SuperCollider, the of rest between each value generation from the patternfunc.
    //! @param newInput A variable reference to store the output of the pattern generation.
    //! @param beg Lower bounds for the Pwhite engine.
    //! @param beg Upper bounds for the Pwhite engine.
    void m_Tick(std::function<float(float&, float&)> func, std::chrono::duration<float> evaltime, unsigned long& milliseconds, std::atomic<float>& newInput, float& beg, float& end);
};
