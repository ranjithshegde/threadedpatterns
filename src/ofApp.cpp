#include "ofApp.h"
#include "ofGraphics.h"

//--------------------------------------------------------------
//! Setup the sound stream with given properties & start the pattern egnine using `Patterns::Play`
//! ```cpp
//! patterns.Play(std::bind(&Patterns::Pwhite, std::ref(patterns), std::placeholders::_1, std::placeholders::_2), evaltime, std::ref(millis), std::ref(output), beg, end);
//! ```
//! This call links the pwhite engine with the pattern generation function using `std::atomics`, `std::bind` and `std::placeholders` to communicate with pattern on separate thread asynchronously.
void ofApp::setup()
{
    freq = 300;
    phase = 0;
    sampleRate = 48000;
    bufferSize = 512;
    phaseInc = (TWO_PI * freq) / static_cast<double>(sampleRate);
    oscStart = false;
    amplitude = 0.1;

    // freq2 = 300;
    // phase_2 = 0;
    // phase_2Inc = (TWO_PI * freq2) / (double)sampleRate;
    // amp2 = 0.1;

    ofSoundStreamSettings settings;
    settings.setOutListener(ofGetAppPtr());
    settings.sampleRate = sampleRate;
    settings.bufferSize = bufferSize;
    settings.numOutputChannels = 2;
    settings.numBuffers = 2;
    soundStream.setup(settings);

    // float beg = 120.0F;
    // float end = 2000.F;
    auto evaltime = patterns.evalTime();

    luv.setup(file);
    luv.update();
    a = static_cast<float>(luv.getNumber("a"));
    b = static_cast<float>(luv.getNumber("b"));
    // c = static_cast<float>(luv.getNumber("c"));

    patterns.Play(std::bind(&Patterns::Pwhite, std::ref(patterns), std::placeholders::_1, std::placeholders::_2), evaltime, std::ref(millis), std::ref(output), std::ref(a), std::ref(b));
    oscStart = false;
    end = false;

    grad_a = ofColor(50, 50, 50);
    grad_b = ofColor(200, 200, 200);
}

//--------------------------------------------------------------
void ofApp::update()
{
    luv.update();
    if (luv.getNumber("a") != a) {
        a = static_cast<float>(luv.getNumber("a"));
    }
    if (luv.getNumber("b") != b) {
        b = static_cast<float>(luv.getNumber("b"));
    }
    // if (luv.getNumber("c") != c) {
    //     c = static_cast<float>(luv.getNumber("c"));
    // }
    if (luv.getNumber("dur") != millis) {
        millis = static_cast<unsigned long>(luv.getNumber("dur"));
    }

    if ((ofGetFrameNum()) % 60 == 0) {
        glm::vec3 col1 = luv.getColor("col1", "r", "g", "b");
        grad_a = ofColor(col1.x, col1.y, col1.z);

        glm::vec3 col2 = luv.getColor("col2", "r", "g", "b");
        grad_b = ofColor(col2.x, col2.y, col2.z);

        oscStart = luv.getBool("start");
        end = luv.getBool("stop");

        if (end) {
            exit();
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofBackgroundGradient(grad_a, grad_b);
    // ofDrawCircle(100, 100, c);
    // pLine.draw();
}

//--------------------------------------------------------------
//! A simple oscillator runs based on mouse activity
//! The frequency input for the oscillator is given by `Patterns::Pwhite`
//! This is a callback function that is called every samplerate/bufferzie times a second.
//! During the callback it fills the audio buffer with the phase values from the oscillator and passes it to the hardware sound card.
void ofApp::audioOut(float* buffer, int bufferSize, int nChannels)
{
    for (int i = 0; i < bufferSize; i++) {
        float currentSample = 0;
        if (oscStart) {
            currentSample = sin(phase);
            // frequency = ofMap(ofGetMouseX(), 0, ofGetWidth(), 80, 800);
            freq = output;
            phaseInc = (TWO_PI * freq) / (double)sampleRate;
            phase += phaseInc;
        }
        buffer[i * nChannels + 0] = currentSample * amplitude;
        buffer[i * nChannels + 1] = currentSample * amplitude;
    }
}
//--------------------------------------------------------------
//! Change pattern behaviour with key press
//! Space key resumes the pattern.
//! 's' key stops the pattern
//! 'd' key decreas the duration by 100 milliseconds.
//! 'u' key increases the duratiob by 100 milliseconds.
void ofApp::keyPressed(int key)
{
    if (key == ' ') {
        patterns.resume();
    } else if (key == 'd') {
        if (millis - 100 <= 0) {
            millis = 100;
        }
        millis -= 100;
    } else if (key == 's') {
        patterns.stop();
    } else if (key == 'u') {
        millis += 100;
    } else if (key == 'p') {
        glm::vec3 col1 = luv.getColor("col1", "r", "g", "b");
        grad_a = ofColor(col1.x, col1.y, col1.z);
        glm::vec3 col2 = luv.getColor("col2", "r", "g", "b");
        grad_b = ofColor(col2.x, col2.y, col2.z);
        // luv.printStack();
    }
}

//--------------------------------------------------------------
//! Mouse left button press starts the oscillator that uses Pwhite
void ofApp::mousePressed(int x, int y, int button)
{
    oscStart = !oscStart;
    mouseX = x;
    // pLine = new ofPolyline;

    // quad.addVertex(ofVec3f(x, y));
    pLine.addVertex(x, y);
    // pLine.close();
}

//--------------------------------------------------------------
//! Mouse left button release stops the oscillator that uses Pwhite
void ofApp::mouseReleased(int x, int y, int button)
{
    oscStart = !oscStart;
    pLine.close();
}

//--------------------------------------------------------------
//! Close soundstream on exit
//! Call destructor and clear all threads
void ofApp::exit()
{
    soundStream.close();
    patterns.~Patterns();
}
