#pragma once

#include "chrono"
#include "filesystem"
#include "functional"
#include "string"
#include "thread"
#include "unordered_map"

enum class FileStatus { created,
    modified,
    erased };

class FileWatcher {
public:
    std::string path_to_watch;
    std::chrono::duration<int, std::milli> delay;

    FileWatcher(std::string path_to_watch, std::chrono::duration<int, std::milli> delay);

    void start(const std::function<void(std::string, FileStatus)>& action);

private:
    std::unordered_map<std::string, std::filesystem::file_time_type> m_Paths;
    bool m_Running = true;

    inline bool m_Contains(const std::string& key)
    {
        auto el = m_Paths.find(key);
        return el != m_Paths.end();
    }
};
