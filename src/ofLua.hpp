#pragma once

#include "glm.hpp"
#include "iostream"
#include "string"
#include <assert.h>
#include <cstdio>

extern "C" {
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
}

class ofLua {

public:
    ofLua();
    explicit ofLua(std::string file);
    ~ofLua();

    void setup(std::string file);

    void update();

    void update(std::string file);

    void close();

    lua_Number getNumber(const char* value);
    bool getBool(const char* value);

    glm::vec3 getColor(const char* name, const char* r, const char* g, const char* b);

    void printStack();

private:
    lua_State* m_Lua;
    std::string m_File;
    bool m_is_open = false;
};
