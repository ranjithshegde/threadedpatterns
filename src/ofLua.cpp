#include "ofLua.hpp"
#include <iostream>
#include <lua.h>

ofLua::ofLua()
{
    m_Lua = luaL_newstate();
    m_is_open = true;
}

ofLua::ofLua(std::string file)
    : m_File(file)
{
    m_Lua = luaL_newstate();
    m_is_open = true;
    luaL_openlibs(m_Lua);
    luaL_dofile(m_Lua, m_File.c_str());
}

void ofLua::setup(std::string file)
{
    m_File = file;
    luaL_dofile(m_Lua, m_File.c_str());
}

void ofLua::update()
{
    luaL_dofile(m_Lua, m_File.c_str());
}

lua_Number ofLua::getNumber(const char* value)
{
    lua_getglobal(m_Lua, value);
    return lua_tonumber(m_Lua, -1);
}

bool ofLua::getBool(const char* value)
{
    lua_getglobal(m_Lua, value);
    return lua_toboolean(m_Lua, -1);
}

glm::vec3 ofLua::getColor(const char* name, const char* r, const char* g, const char* b)
{
    lua_getglobal(m_Lua, name);
    assert(lua_istable(m_Lua, -1));

    lua_pushstring(m_Lua, r);
    lua_gettable(m_Lua, -2);

    float x = lua_tonumber(m_Lua, -1);

    assert(lua_istable(m_Lua, -2));

    lua_pushstring(m_Lua, g);
    lua_gettable(m_Lua, -3);

    float y = lua_tonumber(m_Lua, -1);

    assert(lua_istable(m_Lua, -3));

    lua_pushstring(m_Lua, b);
    lua_gettable(m_Lua, -4);

    float z = lua_tonumber(m_Lua, -1);
    return { x, y, z };
}

void ofLua::close()
{
    if (m_is_open) {
        lua_close(m_Lua);
        m_is_open = false;
    }
}

ofLua::~ofLua()
{
    close();
    std::cout << "Closed lua state" << std::endl;
}

void ofLua::printStack()
{
    int top = lua_gettop(m_Lua);
    for (int i = 1; i <= top; i++) {
        printf("%d\t%s\t", i, luaL_typename(m_Lua, i));
        switch (lua_type(m_Lua, i)) {
        case LUA_TNUMBER:
            printf("%g\n", lua_tonumber(m_Lua, i));
            break;
        case LUA_TSTRING:
            printf("%s\n", lua_tostring(m_Lua, i));
            break;
        case LUA_TBOOLEAN:
            printf("%s\n", (lua_toboolean(m_Lua, i) ? "true" : "false"));
            break;
        case LUA_TNIL:
            printf("%s\n", "nil");
            break;
        default:
            printf("%p\n", lua_topointer(m_Lua, i));
            break;
        }
    }
}
