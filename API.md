# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`ofApp`](#classofApp) | A simple pwhite implementation that sets the frequency of an oscillator.
`class `[`Patterns`](#classPatterns) | A collection of Pattern functions similar to SuperCollider's Pattern/Event library. The main function creates a thread on every call, which runs in the background, sleeping for specified intervals Any pattern engine can be passed to it as a function pointer, which the play function executes

# class `ofApp` 

```
class ofApp
  : public ofBaseApp
```  

A simple pwhite implementation that sets the frequency of an oscillator.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public ofSoundStream `[`soundStream`](#classofApp_1a32f542fc195492272e0bb8bd4858c4fe) | An abstraction layer to communicate with RTAudio and initiate sound stream.
`public int `[`sampleRate`](#classofApp_1abd83953a5dcff8f0deabbfc2a0170d98) | 
`public int `[`bufferSize`](#classofApp_1af573fde175597807abfefd7b1c34c457) | 
`public bool `[`oscStart`](#classofApp_1a1352f1b7fc35722533d5dc43a21cfd3d) | 
`public double `[`phase`](#classofApp_1a3541378f8d96ec43e21cfed44a1c608e) | 
`public double `[`phaseInc`](#classofApp_1aceab8e62d8eddc6d7a588f6788488e4a) | 
`public float `[`frequency`](#classofApp_1a961a01cb5e7e493feff76e5d38933131) | 
`public float `[`amplitude`](#classofApp_1ae8d93c6598d7959ecaa1d30ec07f41ec) | 
`public float `[`mouseX`](#classofApp_1a36fb2738ddd3c8a3d231e86eefb4f7fb) | 
`public ofPolyline `[`pLine`](#classofApp_1ad50e8c263af1b53ae4d512ca0b3f6c3b) | 
`public ofMesh `[`quad`](#classofApp_1ac444fbf2b8fb8ba3d7d515bda8e53c99) | 
`public `[`Patterns`](#classPatterns)` `[`patterns`](#classofApp_1ac7c152efde6d8b459ab4de294e783fa8) | 
`public unsigned long `[`millis`](#classofApp_1ac9507cfdb563fb3b5bad56bbe878361c) | 
`public std::atomic< float > `[`output`](#classofApp_1a0dfc4de21bc5923f29b9a70b332fc894) | Atomics are needed to safely transfer data between multiple threads.
`public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` | Setup the sound stream with given properties & start the pattern egnine using `[Patterns::Play](#classPatterns_1a26db94bb8b1ecc9fcc10956a9f6da18e)`
`public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` | 
`public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` | 
`public void `[`keyPressed`](#classofApp_1a957d3197364bbac8e67eaa4f15b28ad3)`(int key)` | Change pattern behaviour with key press Space key resumes the pattern. 's' key stops the pattern 'd' key decreas the duration by 100 milliseconds. 'u' key increases the duratiob by 100 milliseconds.
`public void `[`mousePressed`](#classofApp_1a2c2ea9c160231e55424dfd98466ef27d)`(int x,int y,int button)` | Mouse left button press starts the oscillator that uses Pwhite.
`public void `[`mouseReleased`](#classofApp_1aa3131f1554fc49eaa9ee0f284e48129b)`(int x,int y,int button)` | Mouse left button release stops the oscillator that uses Pwhite.
`public void `[`audioOut`](#classofApp_1a7dac98eb535b3ac8ab8d1da7fa6f8caa)`(float * buffer,int bufferSize,int nChannels)` | A simple oscillator runs based on mouse activity The frequency input for the oscillator is given by `[Patterns::Pwhite](#classPatterns_1a6a8c51a7c1116dedd881d9f1a01962ff)` This is a callback function that is called every samplerate/bufferzie times a second. During the callback it fills the audio buffer with the phase values from the oscillator and passes it to the hardware sound card.
`public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` | Close soundstream on exit Call destructor and clear all threads

## Members

#### `public ofSoundStream `[`soundStream`](#classofApp_1a32f542fc195492272e0bb8bd4858c4fe) 

An abstraction layer to communicate with RTAudio and initiate sound stream.

#### `public int `[`sampleRate`](#classofApp_1abd83953a5dcff8f0deabbfc2a0170d98) 

#### `public int `[`bufferSize`](#classofApp_1af573fde175597807abfefd7b1c34c457) 

#### `public bool `[`oscStart`](#classofApp_1a1352f1b7fc35722533d5dc43a21cfd3d) 

#### `public double `[`phase`](#classofApp_1a3541378f8d96ec43e21cfed44a1c608e) 

#### `public double `[`phaseInc`](#classofApp_1aceab8e62d8eddc6d7a588f6788488e4a) 

#### `public float `[`frequency`](#classofApp_1a961a01cb5e7e493feff76e5d38933131) 

#### `public float `[`amplitude`](#classofApp_1ae8d93c6598d7959ecaa1d30ec07f41ec) 

#### `public float `[`mouseX`](#classofApp_1a36fb2738ddd3c8a3d231e86eefb4f7fb) 

#### `public ofPolyline `[`pLine`](#classofApp_1ad50e8c263af1b53ae4d512ca0b3f6c3b) 

#### `public ofMesh `[`quad`](#classofApp_1ac444fbf2b8fb8ba3d7d515bda8e53c99) 

#### `public `[`Patterns`](#classPatterns)` `[`patterns`](#classofApp_1ac7c152efde6d8b459ab4de294e783fa8) 

#### `public unsigned long `[`millis`](#classofApp_1ac9507cfdb563fb3b5bad56bbe878361c) 

#### `public std::atomic< float > `[`output`](#classofApp_1a0dfc4de21bc5923f29b9a70b332fc894) 

Atomics are needed to safely transfer data between multiple threads.

#### `public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` 

Setup the sound stream with given properties & start the pattern egnine using `[Patterns::Play](#classPatterns_1a26db94bb8b1ecc9fcc10956a9f6da18e)`
```cpp
patterns.[Play](#classPatterns_1a26db94bb8b1ecc9fcc10956a9f6da18e)(std::bind(&[Patterns::Pwhite](#classPatterns_1a6a8c51a7c1116dedd881d9f1a01962ff), std::ref(patterns), std::placeholders::_1, std::placeholders::_2), evaltime, std::ref(millis), std::ref([output](#classofApp_1a0dfc4de21bc5923f29b9a70b332fc894)), beg, end);
```
 This call links the pwhite engine with the pattern generation function using `std::atomics`, `std::bind` and `std::placeholders` to communicate with pattern on separate thread asynchronously.

#### `public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` 

#### `public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` 

#### `public void `[`keyPressed`](#classofApp_1a957d3197364bbac8e67eaa4f15b28ad3)`(int key)` 

Change pattern behaviour with key press Space key resumes the pattern. 's' key stops the pattern 'd' key decreas the duration by 100 milliseconds. 'u' key increases the duratiob by 100 milliseconds.

#### `public void `[`mousePressed`](#classofApp_1a2c2ea9c160231e55424dfd98466ef27d)`(int x,int y,int button)` 

Mouse left button press starts the oscillator that uses Pwhite.

#### `public void `[`mouseReleased`](#classofApp_1aa3131f1554fc49eaa9ee0f284e48129b)`(int x,int y,int button)` 

Mouse left button release stops the oscillator that uses Pwhite.

#### `public void `[`audioOut`](#classofApp_1a7dac98eb535b3ac8ab8d1da7fa6f8caa)`(float * buffer,int bufferSize,int nChannels)` 

A simple oscillator runs based on mouse activity The frequency input for the oscillator is given by `[Patterns::Pwhite](#classPatterns_1a6a8c51a7c1116dedd881d9f1a01962ff)` This is a callback function that is called every samplerate/bufferzie times a second. During the callback it fills the audio buffer with the phase values from the oscillator and passes it to the hardware sound card.

#### `public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` 

Close soundstream on exit Call destructor and clear all threads

# class `Patterns` 

A collection of Pattern functions similar to SuperCollider's Pattern/Event library. The main function creates a thread on every call, which runs in the background, sleeping for specified intervals Any pattern engine can be passed to it as a function pointer, which the play function executes

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public  `[`Patterns`](#classPatterns_1aa6241d95e039c41cc4f4744d2285ebd9)`()` | Default constuctor.
`public  `[`~Patterns`](#classPatterns_1a91257ce4caca2a7ed65d931cdaeaeb6b)`()` | Destructor that frees all threads.
`public float `[`Pwhite`](#classPatterns_1a6a8c51a7c1116dedd881d9f1a01962ff)`(float begin,float end)` | Pwhite function from SuperCollider. Generates a random number with standard distribution. 
`public void `[`Play`](#classPatterns_1a26db94bb8b1ecc9fcc10956a9f6da18e)`(std::function< float(float, float)> func,std::chrono::duration< float > evaltime,unsigned long & milliseconds,std::atomic< float > & newInput,float beg,float end)` | Starts the pattern engine by first creating a new thread, and then passing the values to `Patterns::m_Tick`
`public void `[`stop`](#classPatterns_1a76a597b4ca8c444657f198d16c66794b)`()` | Stop the pattern execution. Simply sleeps the thread.
`public void `[`resume`](#classPatterns_1a263165c5c7435b994a9b649d22d9f486)`()` | Resume pattern execution.
`public dur_proxy & `[`evalTime`](#classPatterns_1ac55fe91b5648c9c284bb8adb9312f9f4)`()` | Chrono timepoint to store the evaluation time of the pattern function.

## Members

#### `public  `[`Patterns`](#classPatterns_1aa6241d95e039c41cc4f4744d2285ebd9)`()` 

Default constuctor.

#### `public  `[`~Patterns`](#classPatterns_1a91257ce4caca2a7ed65d931cdaeaeb6b)`()` 

Destructor that frees all threads.

#### `public float `[`Pwhite`](#classPatterns_1a6a8c51a7c1116dedd881d9f1a01962ff)`(float begin,float end)` 

Pwhite function from SuperCollider. Generates a random number with standard distribution. 
#### Parameters
* `begin` Lower bounds. 

* `end` Upper bounds.

#### `public void `[`Play`](#classPatterns_1a26db94bb8b1ecc9fcc10956a9f6da18e)`(std::function< float(float, float)> func,std::chrono::duration< float > evaltime,unsigned long & milliseconds,std::atomic< float > & newInput,float beg,float end)` 

Starts the pattern engine by first creating a new thread, and then passing the values to `Patterns::m_Tick`
#### Parameters
* `func` A std:;function or a function pointer - The pattern engine to execute on the new thread. It needs the relevant arguments for that function. 

* `evaltime` A time point instance that represents the duration of execution of the pattern function. Ideally this should be returned directly from the pattern generator. 

* `milliseconds` Same as Dur value from SuperCollider, the of rest between each value generation from the patternfunc. 

* `newInput` A variable reference to store the output of the pattern generation. 

* `beg` Lower bounds for the Pwhite engine. 

* `beg` Upper bounds for the Pwhite engine.

#### `public void `[`stop`](#classPatterns_1a76a597b4ca8c444657f198d16c66794b)`()` 

Stop the pattern execution. Simply sleeps the thread.

#### `public void `[`resume`](#classPatterns_1a263165c5c7435b994a9b649d22d9f486)`()` 

Resume pattern execution.

#### `public dur_proxy & `[`evalTime`](#classPatterns_1ac55fe91b5648c9c284bb8adb9312f9f4)`()` 

Chrono timepoint to store the evaluation time of the pattern function.

Generated by [Moxygen](https://sourcey.com/moxygen)