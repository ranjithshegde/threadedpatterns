# Async Audio Pattern Library

_Notice: This project is very experimental. The API is subject to change with
noitce whereas the code is subject to change without Notice_

For a quickstart documentation, follow this file: [API](API.md)

For a more complete documentation, please click
[here](https://ranjithshegde.gitlab.io/threadedpatterns)
